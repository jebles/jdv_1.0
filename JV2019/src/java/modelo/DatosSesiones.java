package modelo;
/** * Clase principal que contiene dos vector de objetos de la clase Usuario
 * y SesionUsuario *  Contiene un método para importar datos desde un texto
 * etiquetado. *  Las pruebas se organizan desde main()
 *
 *
 * @source: DatosSesiones.java * @version: 1.4 - 18/01/2016
 * @author: ajp
 */
public class DatosSesiones { // Vectores de objetos 

    public static Usuario[] datosUsuarios = new Usuario[10] ;
    public static SesionUsuario[] datosSesiones = new SesionUsuario[20]  ;

    public static void main(String[] args) {
// Prepara texto para la prueba con una cantidad arbitraria de objetos SesionUsuario . 
// Se supone que los usuarios en las sesiones no se repiten. 
// Cada SesionUsuario va precedida por la etiqueta: <sesion> 
// Cada sesión está compuesta por: 
//      - Un Usuario, va precedido por la doble etiqueta: <atrib><atrib> 
//      - Una fecha de sesión va precedida por la etiqueta: <fecha> 
// Cada atributo del Usuario anidado de Usuario va precedido por la etiqueta: <atrib> 
        String textoDatos
                = "<sesion>"
                	+"<usr>"
                		+ "<atrib>0344556K</atrib>"
                		+ "<atrib>pepe0</atrib>"
                		+ "<atrib>López Pérez0</atrib>"
                		+ "<atrib>C/Luna, 27 30132 Murcia</atrib>"
                		+ "<atrib>pepe0@gmail.com</atrib>"
                		+ "<atrib>1990.11.12</atrib>"
                		+ "<atrib>2014.12.3</atrib>"
                		+ "<atrib>miau0</atrib>"
                		+ "<atrib>usuario normal</atrib>"
                	+"</usr>"
                	+ "<fecha>13.01.2015</fecha>"
                +"</sesion>"
                +"<sesion>"
                	+"<usr>"
                		+ "<atrib>0344556K</atrib>"
                		+ "<atrib>pepe0</atrib>"
                		+ "<atrib>López Pérez0</atrib>"
                		+ "<atrib>C/Luna, 27 30132 Murcia</atrib>"
                		+ "<atrib>pepe0@gmail.com</atrib>"
                		+ "<atrib>1990.11.12</atrib>"
                		+ "<atrib>2014.12.3</atrib>"
                		+ "<atrib>miau0</atrib>"
                		+ "<atrib>usuario normal</atrib>"
                	+"</usr>"
                	+ "<fecha>13.01.2015</fecha>"
                 +"</sesion>"
                 +"<sesion>"
                  	+"<usr>"
                  		+ "<atrib>0344556K</atrib>"
                  		+ "<atrib>pepe0</atrib>"
                  		+ "<atrib>López Pérez0</atrib>"
                  		+ "<atrib>C/Luna, 27 30132 Murcia</atrib>"
                  		+ "<atrib>pepe0@gmail.com</atrib>"
                  		+ "<atrib>1990.11.12</atrib>"
                  		+ "<atrib>2014.12.3</atrib>"
                  		+ "<atrib>miau0</atrib>"
                  		+ "<atrib>usuario normal</atrib>"
                  	+"</usr>"
                  	+ "<fecha>13.01.2015</fecha>"
                  +"</sesion>";
                  	
             
        //Llamada al método de importación de datos. 
        importarDatosEtiquetados(textoDatos);
        for (SesionUsuario sesion : datosSesiones) {
            System.out.println("[Sesión]\n" + sesion + "\n");
        }
    }

    /**
     * Importa datos desde un texto etiquetado. Los datos representan objetos la
     * clase SesionUsuario que quedan almacenados en un nuevo * array de tamaño
     * adaptado al número de objetos a importar, con un margen extra de 20. *
     * Cada SesionUsuario incluye un Usuario no repetido que también queda
     * almacenado en otro nuevo array de tamaño adaptado al número de objetos a
     * importar, con un margen extra de 10. Se supone que los usuarios en las
     * sesiones no se repiten. Cada objeto SesionUsuario se separa con la
     * etiqueta: <sesion>
     * Para cada SesionUsuario: Un Usuario se separa con la doble etiqueta:
     * <atrib><atrib>
     * Una fecha se separa con la etiqueta: <fecha>
     * Cada atributo del Usuario anidado se separa con la etiqueta: <atrib>
     *
     * @param texto - con los datos a importar.
     */
    private static void importarDatosEtiquetados(String texto) {
    	
        String[] sesiones = texto.split("<sesion>|</sesion>");
// Dimensiona el número de elementos de los array de datos.        
        datosSesiones = new SesionUsuario[sesiones.length + 20];
        datosUsuarios = new Usuario[sesiones.length + 10];
// Procesa cada sesión . 
        for (int i = 1, j = 0; i < datosSesiones.length; i+=2, j++) {
// Los datos útiles quedan a partir del segundo elemento del array.            
            String[] usr = sesiones[i].split("<usr>|</usr>");
// Cada atributo del usuario se separa en un array.            
            String[] atributos = usr[i].split("<atrib>|</atrib>");
// Crea elementos de los arrays.            
            datosUsuarios[i] = new Usuario();
            datosSesiones[i] = new SesionUsuario();
// Asigna valores de atributos.
            j++;
            datosUsuarios[j].setNif(atributos[0]);
            System.out.println("nif "+j);
            datosUsuarios[j].setNombre(atributos[1]);
            System.out.println("nif "+j);
            datosUsuarios[i].setApellidos(atributos[2]);
            datosUsuarios[i].setDomicilio(atributos[3]);
            datosUsuarios[i].setCorreo(atributos[4]);
            datosUsuarios[i].setFechaNacimiento(atributos[5]);
            datosUsuarios[i].setFechaAlta(atributos[6]);
            datosUsuarios[i].setClaveAcceso(atributos[7]);
            datosUsuarios[i].setRol(atributos[8]);
            System.out.println("rol "+j);
// Los datos útiles quedan a partir del segundo elemento del array.            
            String[] fecha = sesiones[i].split("<fecha>|</fecha>");
// Asigna valores de los atributos.            
            datosSesiones[j].setUsr(datosUsuarios[j]);
            datosSesiones[j].setFecha(fecha[j]);
        }
    }

}
