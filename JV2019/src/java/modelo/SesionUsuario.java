/** 
 * Proyecto: Juego de la vida.
 *  Implementa el concepto de SesionUsuario según el modelo1.1
 *  @since: prototipo1.0
 *  @source: SesionUsuario.java 
 *  @version: 1.1 - 2019/01/25
 *  @author: ajp
 */

package modelo;

import util.Fecha;

public class SesionUsuario {

	private Usuario usr;
	private String fecha; 

	/**
	 * Constructor convencional. Utiliza métodos set...()
	 * @param usr
	 * @param fecha
	 */
	public SesionUsuario(Usuario usr, String fecha) {
		setUsr(usr);
		setFecha(fecha);
	}

	/**
	 * Constructor por defecto. Utiliza constructor convencional.
	 */
	public SesionUsuario() {
		this(new Usuario(), new String());
	}

//	/**
//	 * Constructor copia.
//	 * @param sesion
//	 */
//	public SesionUsuario(SesionUsuario sesion) {
//		this.usr = new Usuario(sesion.usr);
//		this.fecha = new String();
//	}

	public Usuario getUsr() {
		return usr;
	}

	public void setUsr(Usuario usr) {
		assert usr != null;
		this.usr = usr;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha2) {
		assert fecha2 != null;
		this.fecha = fecha2;
	}

//	public String getIdSimulacion() {	
//		return this.usr.getIdUsr() ;
//	}
	
	/**
	 * Redefine el método heredado de la clase Object.
	 * @return el texto formateado del estado (valores de atributos) 
	 * del objeto de la clase SesionUsuario  
	 */
	@Override
	public String toString() {
		return usr.toString() 
				+ String.format("%s\n", fecha);	
	}

} // class
