import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/** 
 * Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 
 * En esta versión sólo se ha aplicado un diseño OO básico.
 *  Se pueden detectar varios defectos y antipatrones de diseño:
 *  	- Obsesión por los tipos primitivos.
 *      - Exceso de métodos estáticos.
 *  	- Clase acaparadora, que delega poca responsabilidad. 
 *  	- Clase demasiado grande.
 * @since: prototipo1.0
 * @source: JVPrincipal.java 
 * @version: 1.0 - 2018/11/30
 * @author: 4004010
 */

public class JVPrincipal {

	private static final int MAX_INTENTOS_PERMITIDOS = 3;

	public static void main(String[] args) {
		
		//almacén de datos del programa
		Usuario[] datosUsuarios = new Usuario[10];
		SesionUsuario[] datosSesiones = new SesionUsuario[10];
		
		int sesionesRegistradas = 0; //contador de sesiones registradas
		
		
			cargarUsuariosPrueba();
			mostrarTodosUsuarios();
			
			if (inicioSesionCorrecto()) {
				registrarSesion();
				System.out.println("Bienvenido");
				new Simulacion.arrancarDemo();
			}
			
			else {
				System.out.println("Demasiados intentos fallidos.");
			}
			System.out.println("Fin del programa.");	
	}

	private static void registrarSesion() {
		// TODO Auto-generated method stub
		
	}

	private static void mostrarTodosUsuarios() {
		
		
	}

	private static void cargarUsuariosPrueba() {
			/*  Carga datos de prueba en el vector de datosUsuarios teniendo en cuenta que:
             *  Se deben cargar usuarios con valores diferentes en cada posición del vector utilizando un bucle que se repite las veces necesarias.
             *  Se debe hacer un volcado completo por pantalla de todos los usuarios almacenados en el vector de datosUsuarios. 
             *  Se debe programar un método que se llame mostrarTodosDatosUsuarios().
			 */
		for (int i = 1; i < 10; i++) { //bucle para crear 10 usuarios aleatorios
			
			String letrasRdm = Character.toString((char) i)+Character.toString((char) ((char) i+Math.random()*10));
			
			Usuario usr = new Usuario( letrasRdm, letrasRdm, " "," ", " ", new GregorianCalendar(2000, i, 21), new GregorianCalendar(2000, 02, i+i), " ", " " );
			//Usuario usr = new Usuario();
		}
	}

	private static boolean inicioSesionCorrecto() {
		Scanner teclado = new Scanner(System.in);
		int intentos_permitidos = MAX_INTENTOS_PERMITIDOS;
			
		return false;
	}

} //class
