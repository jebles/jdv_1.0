/** 
 * Proyecto: Juego de la vida.
 * Organiza aspectos de gestión de la simulación según el modelo1. HAy que hacer mundos diferenctes para cada usuario.
 * En esta versión sólo se ha aplicado un diseño OO básico.
 * Se pueden detectar varios defectos y antipatrones de diseño:
 *  	- Obsesión por los tipos primitivos.
 *  	- Clase demasiado grande.
 *  	- Clase acaparadora, que delega poca responsabilidad.  
 * @since: prototipo1.0
 * @source: Simulacion.java 
 * @version: 1.0 - 2018.11.21
 * @author: ajp
 */
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Simulacion {
	
	//los que crea en su version:
	private Usuario usr;
	
	private Calendar fecha;
	
	private byte[][] mundo; //el algoritmo final va a ser con 1 y 0
	
	//parameteros para actualizar las reglas del juego
	private int[] valoresSupervivencia;
	private int[] valoresNacimiento;
	
	private static final int TAMAÑO_MUNDO = 18;
	private static final int CICLOS_SIMULACION = 20;
	
	//constructor tipo1
	public Simulacion(Usuario usr, Calendar fecha, byte[][] mundo) {
		setUsr(new Usuario());
		setFecha(new GregorianCalendar());
		setMundo(new byte [TAMAÑO_MUNDO][TAMAÑO_MUNDO]);
		aplicarLeyesEstandar();
	}
	public Usuario getUsr() {
		return usr;
	}
	public void setUsr(Usuario usr) {
		this.usr = usr;
	}
	public Calendar getFecha() {
		return fecha;
	}
	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}
	public byte[][] getMundo() {
		return mundo;
	}
	public void setMundo(byte[][] mundo) {
		this.mundo = mundo;
	}
	
	//en esta version parece que no se usará aun?
	private void aplicarLeyesEstandar() {
		valoresSupervivencia = new int[] {2,3};
		valoresNacimiento = new int[] {3};
	}
	//constructor tipo 2, mas avanzada, eleccion del profesor / this es incompatible con super (?)
	public Simulacion() {
		this(new Usuario(),
				new GregorianCalendar(),
				new byte[TAMAÑO_MUNDO][TAMAÑO_MUNDO]);
	}
	
	public Simulacion(Simulacion simulacion) {
		//this.usr =  simulacion.usr.clone(); //hemos creado un clone en Usuario al final, linea 267 y casteado si fuera un Object	 
		this.usr =  new Usuario(simulacion.usr);
		this.fecha =  (Calendar) simulacion.fecha.clone(); //obtiene un objeto replica independiente  // se castea (Calendar) porque es un Object el clon
		
		//matriz que representa al mundo, en la siguiente version irá a una clase
		this.mundo = new byte[simulacion.mundo.length] [simulacion.mundo.length];	
						  	
		for (int i=0; i < simulacion.mundo.length; i++) { //itera fila a fila la matriz
							
			/**
			  Parameters arraycopy
				@src the source array.
				@srcPos starting position in the source array.
				@dest the destination array.
				@destPos starting position in the destination data.
				@length the number of array elements to be copied.
			 */				//origen			//inicio
			System.arraycopy(simulacion.mundo[i], 0, 
							//destino	  //inicio copia en destino
							this.mundo[i], 0, 
							//tamaño copiado
							simulacion.mundo[i].length); //arraycopy permite copiar datos dentro de una matriz y entre matrices
		}
				
	}

	public static String[] arrancarDemo(String[] dish){
		
		String[] newGen= new String[dish.length];
		for(int row= 0;row < dish.length;row++){//each row
			newGen[row]= "";
			for(int i= 0;i < dish[row].length();i++){//each char in the row
				String above= "";//neighbors above
				String same= "";//neighbors in the same row
				String below= "";//neighbors below
				if(i == 0){//all the way on the left
					//no one above if on the top row
					//otherwise grab the neighbors from above
					above= (row == 0) ? null : dish[row - 1].substring(i,
									i + 2);
					same= dish[row].substring(i + 1, i + 2);
					//no one below if on the bottom row
					//otherwise grab the neighbors from below
					below= (row == dish.length - 1) ? null : dish[row + 1]
									.substring(i, i + 2);
				}else if(i == dish[row].length() - 1){//right
					//no one above if on the top row
					//otherwise grab the neighbors from above
					above= (row == 0) ? null : dish[row - 1].substring(i - 1,
									i + 1);
					same= dish[row].substring(i - 1, i);
					//no one below if on the bottom row
					//otherwise grab the neighbors from below
					below= (row == dish.length - 1) ? null : dish[row + 1]
									.substring(i - 1, i + 1);
				}else{//anywhere else
					//no one above if on the top row
					//otherwise grab the neighbors from above
					above= (row == 0) ? null : dish[row - 1].substring(i - 1,
									i + 2);
					same= dish[row].substring(i - 1, i)
									+ dish[row].substring(i + 1, i + 2);
					//no one below if on the bottom row
					//otherwise grab the neighbors from below
					below= (row == dish.length - 1) ? null : dish[row + 1]
									.substring(i - 1, i + 2);
				}
				int neighbors= getNeighbors(above, same, below);
				if(neighbors < 2 || neighbors > 3){
					newGen[row]+= "_";//<2 or >3 neighbors -> die
				}else if(neighbors == 3){
					newGen[row]+= "#";//3 neighbors -> spawn/live
				}else{
					newGen[row]+= dish[row].charAt(i);//2 neighbors -> stay
				}
			}
		}
		return newGen;
	}
 
	public static int getNeighbors(String above, String same, String below){
		int ans= 0;
		if(above != null){//no one above
			for(char x: above.toCharArray()){//each neighbor from above
				if(x == '#') ans++;//count it if someone is here
			}
		}
		for(char x: same.toCharArray()){//two on either side
			if(x == '#') ans++;//count it if someone is here
		}
		if(below != null){//no one below
			for(char x: below.toCharArray()){//each neighbor below
				if(x == '#') ans++;//count it if someone is here
			}
		}
		return ans;
	}
 
	public static void print(String[] dish){
		for(String s: dish){
			System.out.println(s);
		}
	}
	
	
} //class
